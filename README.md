Homie Server Misc plugin
========================

The HSM plugin is a somewhat old idea that I had a while ago, where I thought that we
wouldn't need each one class function to have it's own plugin, so I made this instead,
where I made all the different "plugins" to be subplugins instead.

As well as the methods listed below on this page, the subplugins have access to the MetaManager,
MessageUtil and PluginVocab classes. Some more information about them below.

Base commands
-------------
*/hsm* - Lists the current plugin version as well as the loaded subplugins.

*/hsm reload* - Reloads all subplugins configurations.

Subplugin Overview
------------------
*public boolean enabled*

If *false*, the plugin will not try to register it's events, or to run it's onCommand method.

*public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args)*

Used in the same way as the normal onCommand function in JavaPlugin.

*public void onConfigurationLoad(Configuration config)*

Run when HSM tries to load each subplugin's configuration.

*public void onDisable()*

Run when HSM is getting disabled.

*public abstract String getPluginName()*

Should return the plugin's name.

*public abstract String getAuthorString()*

Should return the authors name(s).

*public void writeHelp(CommandSender sender, String[] args)*

Used when the CommandSender wishes to view the help of a subplugin. Usually a status page of some sorts.

MetaManager
-----------
A símple class that easily gets and sets meta values of each player on the server,
with a key so you can find your key easily.

**Methods**

*Object getMeta(Player player, String key)* - Get's a metavalue of a specific player. Returns null if it cannot find any.

*void setMeta(Player player, String key, Object value)* - Sets a metavalue of a specific player.

PluginVocab
-----------
A class that get's strings from the vocab.yml file. Useful if someone wants to change any string in the subplugins.
Preferably to be loaded in once when the plugin is getting loaded or when the configuration loads.

*String getVocab(String vocabAddress)*

Simply returns the specified vocab address.

*String getVocab(String vocabAddress, HashMap<String, String> variables)*

Replaces all specified variables with the specified values, and returns the String.

*String getVocab(String vocabAddress, HashMap<String, String> variables, boolean replaceColors*)

Replaces all variables with the specified values, and properly replaces color codes with the correct colors.

MessageUtil
-----------
Does basically the same as the plugin vocab does, but without the String needing to be in the vocab.yml file.

*static String replaceColors(String s)*

Replaces all colorcodes with the correct colors.

*static String replaceVars(String msg, HashMap<String, String> variables)*

Replaces all specified variables with a specified value.

Current Subplugins
------------------

RandomTeleport
--------------
Let's users teleport randomly within pre-set boundaries of a world, currently only one boundary per world.

**Commands**

*/tprandom* - Let's the player teleport randomly.

*/tprandom <player>* - Let's a player teleport another player randomly.

HopperBlocker
-------------
Blocks hopper transactions from any container that is locked using Lockette.
Does allow hopper transactions if both the hopper and above container is protected by the same player.
Was made because Lockette did not have this feature itself, do not know if it's still needed. 

HidePlayers
-----------
Gives each player an item that allows them to toggle the visibility of all other players.
While all players are hidden, it does not allow any items to be dropped or picked up.

**This plugin have not been tester with multiple players**

Current items used: Redstone dust and Glowstone dust with the damage value 2.

Damage value 2 used so it cannot be gotten through survival or the creative menu.

ChatCorrector
-------------
If a player sends a message that is more capitalized than allowed in the configuration, it will automatically
rebuild the string with correct capitalization.
There is also an option to have it remove IP's and web addresses from a sent message.

Does also have the ReplaceNames functionality that replaces any mentioned usernames with their displayname/nickname,
however it is currently unused due to to a bug where the text after the username always came out white, which was not wanted.

ToggleKillfeed
--------------
Let's each player choose if they want to have the killfeed turned on or not, or only show kills from within a certain radius.

**Commands**

*/killfeed [mode]* - Toggles between killfeed modes or sets a mode. Valid modes are *Show*/*Local*/*Hide*.

**This plugin have not been tested with multiple players**