package com.homieserver.homieservermisc;

import java.io.*;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Project: HomieServerMisc
 * User: Stripe103
 * Date: 2013-07-03
 * Time: 13:04
 */
public class HomieServerMisc extends JavaPlugin {

    public PluginManager pm;
    public Logger log;
    private SubPlugin[] subPlugins;

    File configFile;
    public Configuration config;

    @Override
    public void onEnable()
    {
        log = this.getLogger();
        pm = getServer().getPluginManager();

        PluginVocab.getInstance().setup(this);
        MetaManager.getInstance().setup(this);
        LoadConfiguration();
    }

    private void copy(InputStream in, File file)
    {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len=in.read(buf))>0)
            {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void LoadConfiguration()
    {
        subPlugins = null;
        config = null;
        configFile = null;

        // Initialize the plugins
        subPlugins = new SubPlugin[] {
                new com.homieserver.homieservermisc.subplugins.RandomTeleport(this),
                new com.homieserver.homieservermisc.subplugins.HopperBlocker(this),
                new com.homieserver.homieservermisc.subplugins.HidePlayers(),
                new com.homieserver.homieservermisc.subplugins.ChatCorrector(),
                new com.homieserver.homieservermisc.subplugins.ToggleKillfeed()
        };

        configFile = new File(getDataFolder(), "config.yml");
        // Load user configuration
        if (!configFile.exists())
        {
            configFile.getParentFile().mkdirs();
            copy(getResource("config.yml"), configFile);
        }

        if (configFile.exists())
            config = YamlConfiguration.loadConfiguration(configFile);

        // Run the loadconfig event on all subplugins
        for (SubPlugin pl : subPlugins)
            pl.onConfigurationLoad(config);

        for (SubPlugin pl : subPlugins)
            if (pl.enabled)
                pm.registerEvents(pl, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args)
    {
        boolean ret = false;

        if (cmd.getName().equalsIgnoreCase("HSM"))
        {
            if (args.length > 0)
            {
                if (args[0].equalsIgnoreCase("RELOAD"))
                {
                    pm.disablePlugin(this);
                    pm.enablePlugin(this);
                    LoadConfiguration();
                    PluginVocab.getInstance().LoadConfig();
                    sender.sendMessage("[HSM] Configuration reloaded!");
                }
                else
                    for (SubPlugin pl : subPlugins)
                        if (pl.enabled)
                            if (pl.getPluginName().equalsIgnoreCase(args[0]))
                                pl.writeHelp(sender);
                return true;
            }
            else
            {
                sender.sendMessage(ChatColor.GREEN + "::" + ChatColor.WHITE + " Homie Server Misc Plugins " + ChatColor.GREEN + "::");
                sender.sendMessage(ChatColor.GREEN + "Version:" + ChatColor.WHITE + " " + this.getDescription().getVersion());
                sender.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GREEN + "/HSM RELOAD" + ChatColor.WHITE + " to reload the configuration.");

                String loadedPluginsString = "";
                if (subPlugins.length > 0)
                {
                    for (int i = 0; i < subPlugins.length; i++)
                    {
                        if (subPlugins[i].enabled)
                        {
                            if (loadedPluginsString == "")
                                loadedPluginsString += subPlugins[i].getPluginName();
                            else
                                loadedPluginsString += ", " + subPlugins[i].getPluginName();
                        }
                    }
                }
                else
                    loadedPluginsString = "None";

                sender.sendMessage(ChatColor.GREEN + "Loaded plugins: " + ChatColor.WHITE + loadedPluginsString);
                return true;
            }
        }

        for (SubPlugin plugin : subPlugins)
            if (plugin.enabled)
                if (plugin.onCommand(sender, cmd, name, args))
                    ret = true;

        return ret;
    }
}
