package com.homieserver.homieservermisc;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import java.util.List;

/**
 * Project: HomieServerMisc
 * User: Stripe103
 * Date: 2013-07-03
 * Time: 13:03
 */
public class MetaManager
{
    Plugin plugin;

    private static MetaManager instance = new MetaManager();
    public static MetaManager getInstance() { return instance; }

    public void setup(Plugin plugin)
    {
        this.plugin = plugin;
    }

    public Object getMeta(Player player, String key)
    {
        List<MetadataValue> values = player.getMetadata(key);
        for (MetadataValue value : values)
        {
            if (value.getOwningPlugin().getDescription().getName().equals(this.plugin.getDescription().getName()))
            {
                return value.value();
            }
        }

        return null;
    }

    public void setMeta(Player player, String key, Object value)
    {
        player.setMetadata(key, new FixedMetadataValue(this.plugin, value));
    }
}
