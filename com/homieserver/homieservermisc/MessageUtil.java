package com.homieserver.homieservermisc;

import org.bukkit.plugin.Plugin;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Project: HomieServerMisc
 * User: Stripe103
 * Date: 2013-07-03
 * Time: 13:03
 */
public class MessageUtil
{
    public static String replaceColors(String s)
    {
        return s.replaceAll("(&([a-fk-or0-9]))", "§$2");
    }

    public static String replaceVars(String msg, HashMap<String, String> variables)
    {
        for (String s : variables.keySet())
        {
            try
            {
                msg = msg.replace("${" + s + "}", (CharSequence)variables.get(s));
            }
            catch (Exception e)
            {
                System.out.println("Failed to replace string vars. Error on " + s);
            }
        }

        return msg;
    }
}
