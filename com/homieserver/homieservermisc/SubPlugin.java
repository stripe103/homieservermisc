package com.homieserver.homieservermisc;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.event.Listener;

/**
 * Project: HomieServerMisc
 * User: Stripe103
 * Date: 2013-07-03
 * Time: 12:52
 */
public abstract class SubPlugin implements Listener {

    public boolean enabled = false;

    public boolean onCommand(CommandSender sender, Command cmd, String name,
                             String[] args) {
        // TODO Auto-generated method stub
        return false;
    }

    public void onConfigurationLoad(Configuration config) { return; }

    public void onDisable() { return; }
    public abstract String getPluginName();
    public abstract String getAuthorString();

    public void writeHelp(CommandSender sender) { sender.sendMessage("This plugin does not have any status command."); }

}
