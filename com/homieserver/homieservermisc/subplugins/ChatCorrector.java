package com.homieserver.homieservermisc.subplugins;

import com.homieserver.homieservermisc.MessageUtil;
import com.homieserver.homieservermisc.MetaManager;
import com.homieserver.homieservermisc.SubPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Planned Features:
 *   AntiSpam:
 *     * Player cannot send a message that seeems like the last one more than the amount of times set in the configuration.
 *     * The player can however send the same message if more time than set in the configuration have passed.  (Use scheduler or the system time?)
 *      * Stop message if it's a percentage more the same as the last one.
 *      * Stop the message if the player sends more than a set amount of times within 5 seconds.
 *
 *
 *   Current features:
 *     Caps corrector:
 *      * If the sent message is more than a percentage caps, it will be corrected by the following preset rules:
 *        - All letters in the message will be decapitalized.
 *        - First character in the message will be capitalized.
 *        - First character after a dot(.), question mark(?) and an exclamation mark(!) will be capitalized.
 *        - a single "i" as it's own word will be capitalized.
 *        - In a similar way, i'm will be changed to I'm and i'll to I'll.
 *      * The message will not be corrected if it's under the configurable string length, even if it's all capitalized.
 */
public class ChatCorrector extends SubPlugin implements Listener
{
    // Correct caps
    boolean correctCapsEnabled;
    int ccMinimumLength;
    double ccCorrectAt;

    boolean blockAddresses;
    boolean allowInvalidIPs;

    @Override
    public String getPluginName() {
        return "ChatCorrector";
    }

    @Override
    public String getAuthorString() {
        return "Stripe103";
    }

    @Override
    public void onConfigurationLoad(Configuration config)
    {
        // Turn the plugin off in case something in this method fails and doesn't properly finish.
        this.enabled = false;

        // Correct caps
        this.correctCapsEnabled = config.getBoolean("chatcorrector.enabled");
        this.ccMinimumLength = config.getInt("chatcorrector.minimumMessageLength");
        this.ccCorrectAt = config.getDouble("chatcorrector.correctAt");

        this.blockAddresses = config.getBoolean("chatcorrector.blockAddresses");
        this.allowInvalidIPs = config.getBoolean("chatcorrector.allowInvalidIPs");

        this.enabled = correctCapsEnabled;


        HashMap<String, Integer> test = new HashMap<String, Integer>();
        for (String key : test.keySet())
        {
            int oldVal = test.get(key);
            test.remove(key);
            test.put(key, oldVal + 1);
        }
    }

    @Override
    public void writeHelp(CommandSender sender)
    {
        sender.sendMessage(ChatColor.GREEN + getPluginName());

        sender.sendMessage(ChatColor.WHITE + "CorrectCaps Enabled: " + ChatColor.GRAY + correctCapsEnabled);
        if (correctCapsEnabled)
        {
            sender.sendMessage(ChatColor.GRAY + "  minimumLength: " + ChatColor.DARK_GRAY + ccMinimumLength);
            sender.sendMessage(ChatColor.GRAY + "      correctAt: " + ChatColor.DARK_GRAY + ccCorrectAt);
            sender.sendMessage(ChatColor.GRAY + " blockAddresses: " + ChatColor.DARK_GRAY + blockAddresses);
            sender.sendMessage(ChatColor.GRAY + "allowInvalidIPs:" + ChatColor.DARK_GRAY + allowInvalidIPs);
        }

        //sender.sendMessage(ChatColor.WHITE + "ReplaceNames Enabled: " + ChatColor.GRAY + replaceNamesEnabled);
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event)
    {
        String message = event.getMessage();

        // CorrectCaps processing
        if (this.correctCapsEnabled)
        {
            // Don't check if the message is lower than the minimum length
            if (message.length() > ccMinimumLength)
            {
                // Check if more than half of the message is in caps
                int upperCaseCount = 0;

                for (int i = 0; i < message.length(); i++)
                {
                    for (char c = 'A'; c <= 'Z'; c++)
                    {
                        if (message.charAt(i) == c)
                            upperCaseCount++;
                    }
                }

                double calc = (double)upperCaseCount / (double)message.length();

                if (correctCapsEnabled && !event.getPlayer().hasPermission("homieservermisc.decaps.bypass"))
                    if (calc >= ccCorrectAt)
                        message = CorrectCapitalizedString(message);
            }
        }

        // Name Replacement processing
        /*if (this.replaceNamesEnabled)
        {
            message = PlayerDisplayNames(event.getPlayer(), message);
        } */

        if (this.blockAddresses)
        {
            message = message.replaceAll("^(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])$", "<deleted IP>");
            if (!allowInvalidIPs)
                message = message.replaceAll("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", "<deleted IP>");
            message = message.replaceAll("(http://|https://)?([a-zA-Z0-9]+\\.[a-zA-Z0-9\\-]+|[a-zA-Z0-9\\-]+)\\.[a-zA-Z\\.]{2,6}(/[a-zA-Z0-9\\.\\?=/#%&\\+-]+|/|)", "<deleted link>");
        }

        event.setMessage(message);
    }

    private double stringsEqual(String a, String b)
    {
        double hits = 0;

        for (String aStr : a.split(" "))
        {
            if (b.contains(aStr))
            {
                hits += 1;
            }
        }

        return (hits / b.split(" ").length);
    }

    private String ReplacePlayerDisplayNames(Player sender, String str)
    {
        Player[] players = Bukkit.getOnlinePlayers();

        for (Player p : players)
        {
            str = str.replaceAll(p.getName(), p.getName());
        }

        // Loop through each player name and replace any occurancens of it to their display name
        for (Player p : players)
        {
            str = str.replaceAll(p.getName(), p.getDisplayName());
        }

        return str;
    }

    private String CorrectCapitalizedString(String str)
    {
        // Build the new string
        String retString = "";
        boolean nextCapital = true;
        char c;

        for (int i = 0; i < str.length(); i++)
        {
            c = str.charAt(i);

            if (c >= 'A' && c <= 'Z')
            {
                if (nextCapital)
                {
                    String nextChar = "";
                    nextChar += c;
                    retString += nextChar.toUpperCase();
                    nextCapital = false;
                }
                else
                {
                    String nextChar = "";
                    nextChar += c;
                    retString += nextChar.toLowerCase();
                }
            }
            else
                retString += c;

            if (c == '.' || c == '!' || c == '?')
                nextCapital = true;
        }

        // Replace some set strings
        retString = retString.replaceAll(" i ", " I ");
        retString = retString.replaceAll("i\'m", "I\'m");
        retString = retString.replaceAll("i\'ll", "I\'ll");

        return retString;
    }

    private String getLastCharFormat(Player player, String msg)
    {
        String lastColor = "";
        boolean bold = false,
                strikethrough = false,
                underline = false,
                italic = false;
        boolean obfuscated = false;
        boolean checkNext = false;

        for (int i = 0; i < msg.length(); i++)
        {
            char c = msg.charAt(i);

            if (checkNext)
            {
                if (c >= '0' && c <= '9' || c >= 'a' && c <= 'f')
                    lastColor = "&" + c;

                if (c == 'l')
                    bold = true;
                else if (c == 'm')
                    strikethrough = true;
                else if (c == 'n')
                    underline = true;
                else if (c == 'o')
                    italic = true;
                else if (c == 'k')
                    obfuscated = true;
                else if (c == 'r')
                {
                    bold = false;
                    strikethrough = false;
                    underline = false;
                    italic = false;
                    obfuscated = false;
                }

                checkNext = false;
            }
            else
            if (c == '&')
                checkNext = true;
        }

        String lastFormat;
        lastFormat = lastColor;

        if (bold) lastFormat += ChatColor.BOLD;
        if (strikethrough) lastFormat += ChatColor.STRIKETHROUGH;
        if (underline) lastFormat += ChatColor.UNDERLINE;
        if (italic) lastFormat += ChatColor.ITALIC;
        if (obfuscated) lastFormat += ChatColor.MAGIC;

        return lastFormat;
    }
}
