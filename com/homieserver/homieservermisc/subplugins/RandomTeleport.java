package com.homieserver.homieservermisc.subplugins;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.homieserver.homieservermisc.HomieServerMisc;
import com.homieserver.homieservermisc.PluginVocab;
import com.homieserver.homieservermisc.SubPlugin;

/**
 * Teleports any player with the right permission to a random position within set boundaries of the world
 * they are currently in. If the boundaries are not set, the plugin does not teleport.
 *
 * If a player have the right permission, he can also randomly teleport other players.
 *
 */
public class RandomTeleport extends SubPlugin implements Listener {

    int randTeleportTries;
    int maxTeleportTries = 10;

    int depthCheck = 3;

    HomieServerMisc plugin;

    boolean debugmode;

    public RandomTeleport(HomieServerMisc plugin)
    {
        this.plugin = plugin;
        this.enabled = true;
    }

    @Override public String getPluginName() { return "RandomTeleport"; }

    @Override
    public String getAuthorString() {
        return "Stripe103";
    }

    @Override
    public void onConfigurationLoad(Configuration config)
    {
        randTeleportTries = 0;
        maxTeleportTries = config.getInt("tprandom.teleporttries");

        depthCheck = config.getInt("tprandom.depthtocheck");
        debugmode = config.getBoolean("tprandom.debugmode");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args)
    {
        if (this.enabled)
        {
            if (cmd.getName().equalsIgnoreCase("TPRANDOM"))
            {
                // If a player name was entered
                if (args.length > 0)
                {
                    if (sender.hasPermission("homieservermisc.tprandom.other") ||
                            sender instanceof ConsoleCommandSender)
                    {
                        try
                        {
                            Player p = Bukkit.getPlayer(args[0]);
                            if (p.isOnline())
                            {
                                HashMap<String, String> m = new HashMap<String, String>();
                                m.put("player", p.getDisplayName());

                                sender.sendMessage(PluginVocab.getInstance().getVocab("tprandom.teleportingOther", m, true));
                                TeleportRandom(sender, Bukkit.getPlayer(args[0]));
                            }
                            else
                            {
                                HashMap<String, String> variables = new HashMap<String, String>();
                                variables.put("player", args[0]);

                                String vocab = PluginVocab.getInstance().getVocab("tprandom.notonline", variables, true);
                                sender.sendMessage(vocab);
                            }
                        } catch (Exception e)
                        {
                            HashMap<String, String> variables = new HashMap<String, String>();
                            variables.put("player", args[0]);

                            String vocab = PluginVocab.getInstance().getVocab("tprandom.notonline", variables, true);
                            sender.sendMessage(vocab);
                        }
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    if (sender instanceof Player)
                    {
                        TeleportRandom(sender, (Player) sender);
                        return true;
                    }
                    else
                    {
                        sender.sendMessage(PluginVocab.getInstance().getVocab("tprandom.includeplayer", null, true));
                        return true;
                    }
                }
            }
            return false;
        }
        return false;
    }

    private void TeleportRandom(CommandSender sender, Player player)
    {
        randTeleportTries++;

        if (randTeleportTries >= maxTeleportTries)
        {
            randTeleportTries = 0;
            sender.sendMessage(PluginVocab.getInstance().getVocab("tprandom.failed", null, true));
            return;
        }

        // Get the max and min values of the player's world from the configuration
        try
        {
            String[] minimum = plugin.config.getString("tprandom." + player.getWorld().getName() + ".minpos").split(",");
            String[] maximum = plugin.config.getString("tprandom." + player.getWorld().getName() + ".maxpos").split(",");

            int[] min = new int[] { Integer.parseInt(minimum[0]), Integer.parseInt(minimum[1]) };
            int[] max = new int[] { Integer.parseInt(maximum[0]), Integer.parseInt(maximum[1]) };

            // Randomize a X and Z value
            int randX = min[0] + (int)(Math.random() * (((int) max[0] - (int) min[0]) + 1));
            int randZ = min[1] + (int)(Math.random() * (((int) max[1] - (int) min[1]) + 1));

            Location l = GetTeleportPoint(player.getWorld(), randX, randZ);

            if (l.getX() != 0 && l.getY() != 0 && l.getZ() != 0)
            {
                player.teleport(l);
                if (debugmode)
                    sender.sendMessage(ChatColor.LIGHT_PURPLE + "" + l.getX() + "," + l.getY() + "," + l.getZ());
                randTeleportTries = 0;

                // Send a message to the player if the teleport was successful
                if (sender.getName() == player.getName())
                    sender.sendMessage(PluginVocab.getInstance().getVocab("tprandom.success", null, true));
                else
                {
                    HashMap<String, String> m = new HashMap<String, String>();


                    if (sender instanceof ConsoleCommandSender)
                    {
                        m.put("other", ChatColor.GOLD + "Console");
                        player.sendMessage(PluginVocab.getInstance().getVocab("tprandom.successOther", m, true));
                    }
                    else if (sender instanceof Player)
                    {
                        m.put("other", ((Player) sender).getDisplayName());
                        player.sendMessage(PluginVocab.getInstance().getVocab("tprandom.successOther", m, true));
                    }
                }
            } else {
                TeleportRandom(sender, player);
            }
        }
        catch (Exception e)
        {
            sender.sendMessage(PluginVocab.getInstance().getVocab("tprandom.notEnabled", null, true));
            return;
        }
    }

    private Location GetTeleportPoint(World world, int x, int z)
    {
        // Get the highest block
        Block b = world.getHighestBlockAt(x, z);

        // If the block is a grass, dirt, leaf or stone block, assume it's on the top of the world.
        if (b.getType() == Material.GRASS || b.getType() == Material.DIRT || b.getType() == Material.STONE || b.getType() == Material.LEAVES)
            return new Location(b.getWorld(), b.getX() + 0.5, b.getY(), b.getZ() + 0.5);

        // Check if there is at least 3 solid blocks underneath of you
        ConsoleCommandSender s = Bukkit.getConsoleSender();
        Block tempBlock;
        boolean solidblocks = true;

        if (debugmode)
            s.sendMessage(ChatColor.RED + "----------------------------------------------------");

        for (int y = 1; y <= depthCheck; y++)
        {
            tempBlock = world.getBlockAt(b.getX(), b.getY() - y, b.getZ());

            if (debugmode)
            {
                s.sendMessage(ChatColor.GREEN + "Checking block at: " + ChatColor.GOLD + tempBlock.getX() + "," + tempBlock.getY() + "," + tempBlock.getZ());
                s.sendMessage(ChatColor.GREEN + "         Material: " + ChatColor.GOLD + tempBlock.getType().toString());
                if (tempBlock.getType().isSolid())
                {
                    s.sendMessage(ChatColor.GREEN + "          isSolid: " + ChatColor.GOLD + "Yes");
                } else {
                    s.sendMessage(ChatColor.GREEN + "          isSolid: " + ChatColor.GOLD + "No");
                    solidblocks = false;
                }
                s.sendMessage(ChatColor.GREEN + "----------------------------------------------------");
            } else {
                if (!tempBlock.getType().isSolid())
                    solidblocks = false;
            }
        }

        if (solidblocks)
            return new Location(b.getWorld(), b.getX() + 0.5, b.getY(), b.getZ() + 0.5);
        else
            return new Location(world, 0, 0, 0);
    }
}
