package com.homieserver.homieservermisc.subplugins;

import com.homieserver.homieservermisc.MessageUtil;
import com.homieserver.homieservermisc.PluginVocab;
import com.homieserver.homieservermisc.SubPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Adds an "item" to the game which allows the user to hide and show all other players.
 *
 * Current items used:
 * Redstone dust and Glowstone Dust with the damage value 2.
 * Any redstone or glowstone dust with the damage value 2 will be allowed. This can only be spawned or cheated in if not given
 * by this plugin.
 */
public class HidePlayers extends SubPlugin
{
    private List<Player> enabledPlayers = new ArrayList<Player>();

    private final Material offMaterial = Material.REDSTONE;
    private final Material onMaterial = Material.GLOWSTONE_DUST;

    private ItemStack offItem, onItem;

    // Vocab strings
    private String onItemName, onItemDesc,
            offItemName, offItemDesc,
            noDrop, noDropHidden;

    public HidePlayers()
    {
        this.enabled = true;
    }

    @Override
    public String getPluginName() {
        return "HidePlayers";
    }

    @Override
    public String getAuthorString() {
        return "Stripe103";
    }

    @Override
    public void onConfigurationLoad(Configuration config) {
        // Load the plugin vocab strings
        PluginVocab vocab = PluginVocab.getInstance();
        this.onItemName = MessageUtil.replaceColors(vocab.getVocab("hideplayers.onitem.name"));
        this.onItemDesc = MessageUtil.replaceColors(vocab.getVocab("hideplayers.onitem.desc"));
        this.offItemName = MessageUtil.replaceColors(vocab.getVocab("hideplayers.offitem.name"));
        this.offItemDesc = MessageUtil.replaceColors(vocab.getVocab("hideplayers.offitem.desc"));
        this.noDrop = MessageUtil.replaceColors(vocab.getVocab("hideplayers.nodrop"));
        this.noDropHidden = MessageUtil.replaceColors(vocab.getVocab("hideplayers.nodrophidden"));

        // Setup on and off items
        this.offItem = new ItemStack(offMaterial, 1);
        this.offItem.setDurability((short)2);

        ItemMeta m = this.offItem.getItemMeta();
        m.setDisplayName(this.offItemName);
        List<String> l = new ArrayList<String>();
        for (String s : this.offItemDesc.split("\n"))
            l.add(s);
        m.setLore(l);
        this.offItem.setItemMeta(m);

        this.onItem = new ItemStack(onMaterial, 1);
        this.onItem.setDurability((short)2);

        m = this.onItem.getItemMeta();
        m.setDisplayName(this.onItemName);
        l = new ArrayList<String>();
        for (String s : this.onItemDesc.split("\n"))
          l.add(s);
        m.setLore(l);
        this.onItem.setItemMeta(m);

        // Show all players to everyone
        for (Player p : Bukkit.getOnlinePlayers())
            for (Player p2 : Bukkit.getOnlinePlayers())
                p.showPlayer(p2);

        // Set as enabled to the main class
        this.enabled = true;
    }

    public boolean isSameItem(ItemStack a, ItemStack b)
    {
        if (a.getTypeId() != b.getTypeId())
            return false;

        if (a.getDurability() != b.getDurability())
            return false;

        return true;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR))
        {
            if (isSameItem(event.getItem(), this.onItem))
            {
                for (Player p : Bukkit.getOnlinePlayers())
                    if (p != event.getPlayer())
                        event.getPlayer().hidePlayer(p);

                event.getPlayer().getInventory().setItemInHand(this.offItem);

                enabledPlayers.add(event.getPlayer());
            }
            else if (isSameItem(event.getItem(), this.offItem))
            {
                for (Player p : Bukkit.getOnlinePlayers())
                    if (p != event.getPlayer())
                        event.getPlayer().showPlayer(p);

                event.getPlayer().getInventory().setItemInHand(this.onItem);
                enabledPlayers.remove(event.getPlayer());
            }

            event.setCancelled(true);
        }

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getPlayer().getGameMode() == GameMode.CREATIVE)
            event.setCancelled(false);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event)
    {
        if (isSameItem(event.getPlayer().getItemInHand(), this.offItem) || isSameItem(event.getPlayer().getItemInHand(), this.onItem))
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDrop(PlayerDropItemEvent event)
    {
        if (isSameItem(event.getItemDrop().getItemStack(), this.offItem) || isSameItem(event.getItemDrop().getItemStack(), this.onItem))
        {
            event.getPlayer().sendMessage(this.noDrop);
            event.setCancelled(true);
        }
        else
        {
            if (enabledPlayers.contains(event.getPlayer()))
            {
                event.getPlayer().sendMessage(this.noDropHidden);
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerPickup(PlayerPickupItemEvent event)
    {
        // If player have hid other players, don't allow them to pickup items.
        if (enabledPlayers.contains(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event)
    {
        // Unhide all players from the leaving player
        for (Player p : Bukkit.getOnlinePlayers())
        {
            event.getPlayer().showPlayer(p);
            p.showPlayer(event.getPlayer());
        }
        enabledPlayers.remove(event.getPlayer());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        boolean hasItem = false;

        for (ItemStack i : event.getPlayer().getInventory().getContents())
        {
            if (i != null)
            {
                if (isSameItem(i, this.offItem))
                { event.getPlayer().getInventory().remove(i); }

                if (isSameItem(i, this.onItem))
                { hasItem = true; }
            }
        }

        if (!hasItem)
        {
            event.getPlayer().getInventory().addItem(onItem);
        }

        for (Player p : Bukkit.getOnlinePlayers())
        {
            if (this.enabledPlayers.contains(p))
                p.hidePlayer(event.getPlayer());
        }
    }
}
