package com.homieserver.homieservermisc.subplugins;

import com.homieserver.homieservermisc.MessageUtil;
import com.homieserver.homieservermisc.MetaManager;
import com.homieserver.homieservermisc.PluginVocab;
import com.homieserver.homieservermisc.SubPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class ToggleKillfeed extends SubPlugin implements Listener
{
    // Vocab strings
    private String showingString;
    private String localString;
    private String hiddenString;
    private String notValidString;

    int localRange = 100;

    int consoleMode = 0;

    @Override
    public String getPluginName() {
        return "ToggleKillfeed";
    }

    @Override
    public String getAuthorString() {
        return "Stripe103";
    }

    @Override
    public void onConfigurationLoad(Configuration config)
    {
        // Load in the vocab.
        PluginVocab vocab = PluginVocab.getInstance();
        this.showingString = MessageUtil.replaceColors(vocab.getVocab("togglekillfeed.show"));
        this.hiddenString = MessageUtil.replaceColors(vocab.getVocab("togglekillfeed.hide"));
        this.localString = MessageUtil.replaceColors(vocab.getVocab("togglekillfeed.local"));
        this.notValidString = MessageUtil.replaceColors(vocab.getVocab("togglekillfeed.notvalid"));

        this.localRange = config.getInt("killfeedlocalrange");

        this.enabled = true;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args)
    {
        if (cmd.getName().equalsIgnoreCase("killfeed"))
        {
            MetaManager mm = MetaManager.getInstance();

            // If the player specified a mode to switch to.
            if (args.length > 0)
            {
                // If it was an accepted value
                if (args[0].equalsIgnoreCase("SHOW") || args[0].equalsIgnoreCase("LOCAL") || args[0].equalsIgnoreCase("HIDE"))
                {
                    if (sender instanceof Player)
                    {
                        int setMode = 0;
                        if (args[0].equalsIgnoreCase("SHOW"))
                            setMode = 0;
                        else if (args[0].equalsIgnoreCase("LOCAL"))
                            setMode = 1;
                        else if (args[0].equalsIgnoreCase("HIDE"))
                            setMode = 2;

                        mm.setMeta((Player) sender, "togglekillfeed-mode", setMode);
                    }
                    else if (sender instanceof ConsoleCommandSender)
                    {
                        if (args[0].equalsIgnoreCase("SHOW"))
                            this.consoleMode = 0;
                        else if (args[0].equalsIgnoreCase("LOCAL"))
                            this.consoleMode = 1;
                        else if (args[0].equalsIgnoreCase("HIDE"))
                            this.consoleMode = 2;
                    }

                    if (args[0].equalsIgnoreCase("SHOW"))
                        sender.sendMessage(this.showingString);
                    else if (args[0].equalsIgnoreCase("LOCAL"))
                        sender.sendMessage(this.localString);
                    else if (args[0].equalsIgnoreCase("HIDE"))
                        sender.sendMessage(this.hiddenString);

                    return true;
                }
                else
                {
                    sender.sendMessage(this.notValidString);
                    return true;
                }
            }
            else
            {
                // Get the player's current mode
                int killfeedmode = 0;
                if (sender instanceof Player)
                {
                    String r = (String)mm.getMeta((Player) sender, "togglekillfeed-mode");
                    if (r != null)
                        killfeedmode = Integer.parseInt(r);
                }
                else if (sender instanceof ConsoleCommandSender)
                    killfeedmode = this.consoleMode;

                killfeedmode++;

                if (killfeedmode >= 3)
                    killfeedmode = 0;

                switch (killfeedmode)
                {
                    case 0:
                        sender.sendMessage(this.showingString);
                        break;
                    case 1:
                        sender.sendMessage(this.localString);
                        break;
                    case 2:
                        sender.sendMessage(this.hiddenString);
                        break;
                }

                // Save the new mode
                if (sender instanceof Player)
                    mm.setMeta((Player) sender, "togglekillfeed-mode", killfeedmode);
                else if (sender instanceof ConsoleCommandSender)
                    this.consoleMode = killfeedmode;

                return true;
            }
        }

        return false;
    }

    @EventHandler
    public void onEntityDeath(PlayerDeathEvent event)
    {
        String deathMessage = event.getDeathMessage();
        event.setDeathMessage("");

        MetaManager mm = MetaManager.getInstance();

        // Broadcast the message to the proper players.
        for (Player p : Bukkit.getOnlinePlayers())
        {
            int mode = Integer.parseInt((String) mm.getMeta(p, "togglekillfeed-mode"));
            if (mode == 0)
                p.sendMessage(deathMessage);
            else if (mode == 1)
            {
                // Get the distance between this player and the player that died
                double distance = p.getLocation().distance(((Player)event.getEntity()).getLocation());

                if (distance <= this.localRange)
                    p.sendMessage(deathMessage);
            }
        }
    }
}
