package com.homieserver.homieservermisc.subplugins;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Hopper;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.minecart.HopperMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.InventoryHolder;
import org.yi.acru.bukkit.Lockette.Lockette;

import com.homieserver.homieservermisc.HomieServerMisc;
import com.homieserver.homieservermisc.SubPlugin;

/**
 * Blocks all hopper transactions if the container above is protected.
 */
public class HopperBlocker extends SubPlugin implements Listener
{
    HomieServerMisc plugin;

    public HopperBlocker(HomieServerMisc plugin)
    {
        this.plugin = plugin;
    }

    @Override public String getPluginName() { return "HopperBlocker"; }

    @Override
    public String getAuthorString() {
        return "Stripe103";
    }

    @Override
    public void onConfigurationLoad(Configuration config)
    {
        if (Bukkit.getServer().getPluginManager().getPlugin("Lockette") == null)
        {
            plugin.log.log(Level.WARNING, "Lockette not detected. \"HopperBlocker\" disabled.");
            this.enabled = false;
        }
        else
        {
            this.enabled = true;
        }
    }

    @EventHandler
    private void onHopperPickupItem(InventoryMoveItemEvent e)
    {
        if (this.enabled)
        {
            InventoryHolder holder = e.getDestination().getHolder();	// The supposed hopper or hopper minecart
            Block source;		// The container it tries to take from. Putting items into a locked chest via hoppers work.
            Block hopperBlock;
            Location sourceLocation;

            if (holder instanceof HopperMinecart)
            {
                sourceLocation = ((HopperMinecart) holder).getLocation().add(0, 1, 0);
                source = sourceLocation.getWorld().getBlockAt(sourceLocation);
                if (Lockette.isProtected(source) && !Lockette.isEveryone(source))
                    // Block the hopper minecart from taking the item.
                    e.setCancelled(true);
            }
            else if (holder instanceof Hopper)
            {
                sourceLocation = ((Hopper)holder).getLocation().add(0, 1, 0);
                source = sourceLocation.getWorld().getBlockAt(sourceLocation);
                hopperBlock = sourceLocation.getWorld().getBlockAt(sourceLocation.add(0, -1, 0));

                if (!(source instanceof Hopper))
                {

                    if (Lockette.getProtectedOwner(source) != Lockette.getProtectedOwner(hopperBlock) && !Lockette.isEveryone(source))
                    {
                        // Block the hopper from taking the item.
                        e.setCancelled(true);
                    }
                }
            }
            else
                // If it's not a hopper or hopper minecart, stop processing
                return;
        }
    }
}
