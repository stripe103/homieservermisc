package com.homieserver.homieservermisc;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.util.HashMap;

/**
 * Project: HomieServerMisc
 * User: Stripe103
 * Date: 2013-07-03
 * Time: 13:00
 */
public class PluginVocab
{
    private static PluginVocab instance = new PluginVocab();

    public static PluginVocab getInstance() { return instance; }

    private File configFile;
    private YamlConfiguration config;

    private Plugin plugin;

    public void setup(Plugin plugin)
    {
        this.plugin = plugin;
        LoadConfig();
    }

    public void LoadConfig()
    {
        configFile = new File(plugin.getDataFolder(), "vocab.yml");
        // Load user configuration
        if (!configFile.exists())
        {
            configFile.getParentFile().mkdirs();
            copy(plugin.getResource("vocab.yml"), configFile);
        }

        if (configFile.exists())
            config = YamlConfiguration.loadConfiguration(configFile);
    }

    public void copy(InputStream in, File file)
    {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len=in.read(buf))>0)
            {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getVocab(String vocabAddress)
    {
        return config.getString(vocabAddress);
    }

    public String getVocab(String vocabAddress, HashMap<String, String> variables)
    {
        if (variables != null)
            return MessageUtil.replaceVars(getVocab(vocabAddress), variables);
        else
            return getVocab(vocabAddress);
    }

    public String getVocab(String vocabAddress, HashMap<String, String> variables, boolean replaceColors)
    {
        String s = getVocab(vocabAddress);
        if (variables != null)
            s = MessageUtil.replaceVars(s, variables);

        if (replaceColors)
            s = MessageUtil.replaceColors(s);

        return s;
    }
}
